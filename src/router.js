import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './components/Home.vue'
import Listing from './components/Listing.vue'
import NewListe from './components/NewListe.vue'
import Consultation from './components/Consultation.vue'
import NewElementListe from './components/NewElementListe.vue'

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
         {
             path: '/',
             name: 'home',
             component: Home
         },
        {
            path: '/listing',
            name: 'listing',
            component: Listing
        },
        {
            path: '/new',
            name: 'new',
            component: NewListe
        },
        {
            path: '/list/:id(\\d+)',
            name: 'consultation',
            component: Consultation
        },
        {
            path: '/list/:id(\\d+)/add',
            name: 'newElementListe',
            component: NewElementListe
        },
        {
            path: '*',
            redirect : '/'
        }
    ]
})